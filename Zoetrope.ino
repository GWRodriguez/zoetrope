#define ON HIGH
#define OFF LOW

const int pCtrl1 = 2;
const int pCtrl2 = 3;
const int pEnable = 9;
const int pDirectionSwitch = 4;
const int pStateSwitch = 5;
const int pPot = A0;

int state = 0;
int prevState = 0;
int stateDir = 0;
int prevStateDir = 0;

int motorEnabled = 0;
int motorSpeed = 0;
int motorDir = 1;


void setup() {
  pinMode(pDirectionSwitch, INPUT);
  pinMode(pStateSwitch, INPUT);

  pinMode(pCtrl1, OUTPUT);
  pinMode(pCtrl2, OUTPUT);
  pinMode(pEnable, OUTPUT);

  digitalWrite(pEnable, OFF);
}


void loop() {
  state = digitalRead(pStateSwitch);
  delay(1);
  stateDir = digitalRead(pDirectionSwitch);
  motorSpeed = analogRead(pPot) / 8;

  if (state != prevState && state == ON) {
     motorEnabled = !motorEnabled;
  }

  if (stateDir != prevStateDir && stateDir == ON) {
    motorDir = !motorDir;
  }

  if (motorDir == 1) {
    digitalWrite(pCtrl1, ON);
    digitalWrite(pCtrl2, OFF);
  }
  else {
    digitalWrite(pCtrl1, OFF);
    digitalWrite(pCtrl2, ON);
  }

  if (motorEnabled == 1) {
    analogWrite(pEnable, motorSpeed);
  }
  else {
    analogWrite(pEnable, 0);
  }

  prevState = state;
  prevStateDir = stateDir;

  delay(10);
}











